Career path project
===============

Installing
----------

You need to install the following libraries:
import numpy as np
import pandas as pd
import os

Versions:
----------
numpy==1.19.5
pandas==1.1.5

Usage
-----

`Career path project` takes a job title and returns the best fitted next jobs. 

job1=career_path(job_id,N)
job1.filter_jobs_optimized(job_id,N)
job1.next_jobs

Where job_id: is the id of the job for which you want to know the career path 
and N is the number of suggestions you want to display. 
We recommand that N<=10.